#include <sub.hpp>
#include <mul.hpp>

namespace tcl
{
namespace feature01
{
class Engine
{
public:
    Engine();
    ~Engine();

    //! perform (c-d)*(a+b)
    void process(int a, int b, int c, int d);

    int result() const;

private:
    int _result;
    subtraction::Engine _subtraction;
    multiplication::Engine _multiplication;
};
} // namespace feature01
} // namespace tcl
