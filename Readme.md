## Add algorithm submodule
> git submodule add https://gitlab.com/tutorials4/tcl-repo/algorithms/addition.git algorithms/addition

or

> git submodule add git@gitlab.com:tutorials4/tcl-repo/algorithms/addition.git algorithms/addition

## Checkout algorithm submodule
If you clone the feature repository without --recursive, you may find that the algorithm submodules aren't checkout.
You may checkout the submodule by the following git command:
> git submodule init

> git submodule update

Notes:
Always make sure you have at least the reporter access on the target algorithm repository.


## Learn more from below:

[Git Submodules](https://git-scm.com/book/en/v2/Git-Tools-Submodules)
