#include "feature01.hpp"
#include <add.hpp>

namespace tcl
{
namespace feature01
{
Engine::Engine() : _result(0) {}
Engine::~Engine() = default;

void Engine::process(int a, int b, int c, int d)
{
    addition::Engine addition;
    addition.process(a, b);
    _subtraction.process(c, d);
    _multiplication.process(_subtraction.result(), addition.result());
    _result = _multiplication.result();
}

int Engine::result() const
{
    return _result;
}

} // namespace feature01
} // namespace tcl
