#include <iostream>
#include <feature01.hpp>

int main() {
    std::cout << "Test Start..." << std::endl;
    tcl::feature01::Engine feature;

    feature.process(3,1,10,6);
    std::cout << feature.result() << std::endl;
    if (feature.result() != 16) {
        return 1;
    }
    return 0;
}
